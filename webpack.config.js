const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require('path');

module.exports = {
    entry: './src/App.tsx',
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ],
                exclude: /\.module\.css$/
            },
            {
                test: /.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader'
                    },
                ],
            },
            {
                test   : /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader : 'babel',
                query  : {
                  presets:[ 'react', 'es2015' ]
                }
              }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Webpack Output",
        }),
        extractSass
    ],
    devServer: {
        contentBase: './dist',
        open: true
    },
};