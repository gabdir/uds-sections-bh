describe('Section page testing', function() {
    beforeEach(() => {
        cy.visit('/');
    })
    it('just works', function() {
    })

    it('has navbar', () => {
        cy.get('[data-cy="navbar"]').should('have.length', 1)
    })

    it('has title', () => {
        cy.contains('h2', 'Секции в Москве')
        cy.contains('h3', 'По алфавиту')
    })

    it('has footer', () => {
        cy.get('[data-cy="footer"]').should('have.length', 1)
    })    

    // it('has section list', () => {
    //     cy.get('[data-cy="sectionId"]').should('have', 'div').as('sectionId')
        
    //     cy.get('@sectionId')
    //         .first()
    //         .should('have.attr', 'key')
    // })
})