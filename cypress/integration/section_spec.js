describe('Section page testing', function() {
    beforeEach(() => {
        cy.visit('/1');
    })
    it('just works', function() {
    })

    it('has navbar', () => {
        cy.get('[data-cy="navbar"]').should('have.length', 1)
    })

    it('has title', () => {
        cy.contains('h1', 'Мини-футбол')
    })

    it('has footer', () => {
        cy.get('[data-cy="footer"]').should('have.length', 1)
    })

    it('slider works and clickable', () => {
        cy.get('[data-cy="gallary-slider-arrow-left"]').should('have.length', 1).click().click().click()
        cy.get('[data-cy="gallary-slider-arrow-right"]').should('have.length', 1).click()

    })

    // it('has social share', () => {
    //     cy.get('[data-cy="social-link"]').should('have.length', 1).as('socialLinks');
    //     cy.get('@socialLinks')
    //     .first()
    //         .should('have', 'title')
    // })  

    // it('has similar sections', () => {
    //     cy.get('[data-cy="similar-sections"]').should('have.length', 1).as('similarSections');
    //     cy.get('@similarSections')
    //     .first()
    //         .should('have', 'div')
    // })

    
    
})