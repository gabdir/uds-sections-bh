const pkg = require("./package.json");
const path = require('path');

module.exports = {
  apiPath: "stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name.replace("uds-", "")}/${pkg.version}/`
    }
  },
  config: {
    'sections.api.base.url': '/api',
  },
  navigations: {
    news: "/news",
    orgs: "/orgs",
    sections: "/sections", 
    ya: "https://yandex.ru"
  }, 
  externals: {
    'styled-components': 'styled-components',
  },
  resolve: {
    alias: {
      'styled-components': path.resolve("./node_modules", "styled-components"),
    },
  },
  optimization: {
      runtimeChunk: {
        name: "vendor"
    }
  },
};
