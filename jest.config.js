
module.exports = {
    // Automatically clear mock calls and instances between every test
    clearMocks: true,

    // Indicates whether the coverage information should be collected while executing the test
    collectCoverage: true,

    // The directory where Jest should output its coverage files
    coverageDirectory: "coverage",

    moduleNameMapper: {
        "\\.(png|svg|jpg|jpeg)$": "<rootDir>/__mocks__/imageTransformer.js",
        "@ijl.cli": "<rootDir>/__mocks__/cli.js",
        "^@main(.*)$": "<rootDir>/src/$1",
        ".+\\.(css|styl|less|sass|scss)$": "identity-obj-proxy"

    },
    snapshotSerializers: [
        'enzyme-to-json/serializer'
    ],
    modulePathIgnorePatterns: [
        "<rootDir>/cypress/"
    ],
    transformIgnorePatterns: [
        "node_modules/(?!@ngrx|(?!deck.gl)|ng-dynamic)"
    ]
};