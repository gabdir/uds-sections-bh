const sections = {   
    "status": {
        "code": 0
    },
    "sections": [
    {
    "id": 1,
    "name": "Боевые искусства",
    "sections": [
        {
            "id": 1,
            "name": "Айкидо",
            "section_type": 1
        },
        {
            "id": 2,
            "name": "Каратэ",
            "section_type": 1
        },
        {
            "id": 3,
            "name": "Рукопашный бой",
            "section_type": 1
        },
        {
            "id": 4,
            "name": "Таеквон-до",
            "section_type": 1
        },
        {
            "id": 5,
            "name": "Самозащита",
            "section_type": 1
        },
        {
            "id": 6,
            "name": "Самбо",
            "section_type": 1
        },
        {
            "id": 7,
            "name": "Тайский бокс",
            "section_type": 1
        },
        {
            "id": 8,
            "name": "Джиу-джитсу",
            "section_type": 1
        },
        {
            "id": 9,
            "name": "Ушуот",
            "section_type": 1
        },
        {
            "id": 10,
            "name": "Рукопашный бой и основы фехтования коротким и длинным клинком",
            "section_type": 1
        },
        {
            "id": 11,
            "name": "Единоборство «Система»",
            "section_type": 1
        },
        {
            "id": 12,
            "name": "Единоборство «Союз Беркут»",
            "section_type": 1
        }
    ]
},
{
    "id": 2,
    "name": "Общее физическое развитие",
    "sections": [
        {
            "id": 13,
            "name": "ОФП",
            "section_type": 2
        },
        {
            "id": 14,
            "name": "Фитнес",
            "section_type": 2
        },
        {
            "id": 15,
            "name": "Оздоровительная гимнастика",
            "section_type": 2
        },
        {
            "id": 16,
            "name": "Ритмика",
            "section_type": 2
        },
        {
            "id": 17,
            "name": "Многофункциональная гимнастика",
            "section_type": 2
        },
        {
            "id": 18,
            "name": "Тренажерный зал",
            "section_type": 2
        },
        {
            "id": 19,
            "name": "Китайская гимнастика",
            "section_type": 2
        },
        {
            "id": 20,
            "name": "Гимнастика для детей",
            "section_type": 2
        },
        {
            "id": 21,
            "name": "Гиревой спорт",
            "section_type": 2
        },
        {
            "id": 22,
            "name": "Мас-рестлинг",
            "section_type": 2
        }
    ]
},
{
    "id": 3,
    "name": "Теннис",
    "sections": [
        {
            "id": 23,
            "name": "Большой теннис",
            "section_type": 3
        },
        {
            "id": 24,
            "name": "Настольный теннис",
            "section_type": 3
        }
    ]
},
{
    "id": 4,
    "name": "Командные виды спорта",
    "sections": [
        {
            "id": 25,
            "name": "Хоккей",
            "section_type": 4
        },
        {
            "id": 26,
            "name": "Баскетбол",
            "section_type": 4
        },
        {
            "id": 27,
            "name": "Хоккей с шайбой",
            "section_type": 4
        },
        {
            "id": 28,
            "name": "Футбол",
            "section_type": 4
        },
        {
            "id": 29,
            "name": "Волейбол",
            "section_type": 4
        },
        {
            "id": 30,
            "name": "Флорбол",
            "section_type": 4
        },
        {
            "id": 31,
            "name": "Мини-футбол",
            "section_type": 4
        }
    ]
},
{
    "id": 5,
    "name": "Танцы",
    "sections": [
        {
            "id": 32,
            "name": "Хип-Хоп",
            "section_type": 5
        },
        {
            "id": 33,
            "name": "Джаз-Фанк",
            "section_type": 5
        },
        {
            "id": 34,
            "name": "Латино-американские танцы",
            "section_type": 5
        },
        {
            "id": 35,
            "name": "Спортивно-бальные танцы",
            "section_type": 5
        },
        {
            "id": 36,
            "name": "Реггитон",
            "section_type": 5
        },
        {
            "id": 37,
            "name": "Евроденс",
            "section_type": 5
        },
        {
            "id": 38,
            "name": "Спортивные бальные танцы",
            "section_type": 5
        },
        {
            "id": 39,
            "name": "Танцы",
            "section_type": 5
        },
        {
            "id": 40,
            "name": "Хореография",
            "section_type": 5
        },
        {
            "id": 41,
            "name": "Народные танцы",
            "section_type": 5
        },
        {
            "id": 42,
            "name": "Восточные танцы",
            "section_type": 5
        },
        {
            "id": 43,
            "name": "Фламенко",
            "section_type": 5
        }
    ]
},
{
    "id": 6,
    "name": "Лёгкая атлетика",
    "sections": [
        {
            "id": 44,
            "name": "Лёгкая атлетика",
            "section_type": 6
        },
        {
            "id": 45,
            "name": "Скандинавская ходьба",
            "section_type": 6
        },
        {
            "id": 46,
            "name": "Атлетическая гимнастика",
            "section_type": 6
        }
    ]
},
{
    "id": 7,
    "name": "Другие виды спорта",
    "sections": [
        {
            "id": 47,
            "name": "Шахматы",
            "section_type": 7
        },
        {
            "id": 48,
            "name": "Городки",
            "section_type": 7
        },
        {
            "id": 49,
            "name": "Велоспорт",
            "section_type": 7
        },
        {
            "id": 50,
            "name": "Дартс",
            "section_type": 7
        },
        {
            "id": 51,
            "name": "Спортивное ориентирование",
            "section_type": 7
        }
    ]
},
{
    "id": 8,
    "name": "Различные занятия и секции",
    "sections": [
        {
            "id": 52,
            "name": "Творческое развитие",
            "section_type": 8
        },
        {
            "id": 53,
            "name": "Раннее развитие «Дошколёнок»",
            "section_type": 8
        },
        {
            "id": 54,
            "name": "Компьютерная грамотность",
            "section_type": 8
        },
        {
            "id": 55,
            "name": "Занятия для лиц с ограниченными возможностями здоровья (ОВЗ)",
            "section_type": 8
        },
        {
            "id": 56,
            "name": "Надежды маленький оркестрик",
            "section_type": 8
        },
        {
            "id": 57,
            "name": "Клуб исторической реконструкции",
            "section_type": 8
        },
        {
            "id": 58,
            "name": "Рисование",
            "section_type": 8
        },
        {
            "id": 59,
            "name": "Активное долголетие - Танцы",
            "section_type": 8
        },
        {
            "id": 60,
            "name": "Активное долголетие – Йога",
            "section_type": 8
        },
        {
            "id": 61,
            "name": "Раннее физическое развитие с элементами айкидо",
            "section_type": 8
        }
    ]
}
]
}

const section = {   
    "status": {
        "code": 0
    },
    "section": {
        "title": "Мини-футбол", 
    "subTitle" : "С 2006 года базовый центр района по организации досуга и спортивных занятий для жителей. Наш Центр является многофункциональным и многопрофильным досуговым учреждением города Москвы: мы поддерживаем и развиваем все виды творчества, досуговой деятельности и любительского спорта. У Центра есть целых 9 отделений, поэтому до нас удобно добираться любому жителю Южного Бутова.", 
    "image": "Group2.svg",
    "body": {
        "image": "blank-logo.svg",
        "title" :"Что такое мини-футбол и в чём секрет его популярности", 
        "description": "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана! Давайте знакомится, посмотрите видео и если останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24/7 и ответить на все вопросы. Мы предложим спортивные, творческие и развивающие кружки и секции для детей."
    },
    "imgArr": [
        {
            "alt": "Тренажёрный зал",
            "src": "/static/gallery-slider/gallery-photo-1.webp"
        },
        {
            "alt": "Кисти для рисования",
            "src": "/static/gallery-slider/gallery-photo-2.webp"
        },
        {
            "alt": "Тренажерный зал",
            "src": "/static/gallery-slider/gallery-photo-3.webp"
        },
        {
            "alt": "Кабинет детской студии",
            "src": "/static/gallery-slider/gallery-photo-4.webp"
        },
        {
            "alt": "Комната детского досуга",
            "src": "/static/gallery-slider/gallery-photo-5.webp"
        },
        {
            "alt": "Кабинет для детского творчества",
            "src": "/static/gallery-slider/gallery-photo-6.webp"
        },
        {
            "alt": "Зал для танцев",
            "src": "/static/gallery-slider/gallery-photo-7.webp"
        },
        {
            "alt": "Детская студия",
            "src": "/static/gallery-slider/gallery-photo-8.webp"
        },
        {
            "alt": "Комната для занятий с детьми",
            "src": "/static/gallery-slider/gallery-photo-9.webp"
        },
        {
            "alt": "Кабинет военной подготовки",
            "src": "/static/gallery-slider/gallery-photo-10.webp"
        },
        {
            "alt": "Игровая комната",
            "src": "/static/gallery-slider/gallery-photo-11.webp"
        },
        {
            "alt": "Компьютерный класс",
            "src": "/static/gallery-slider/gallery-photo-12.webp"
        },
        {
            "alt": "Студия рисования",
            "src": "/static/gallery-slider/gallery-photo-13.webp"
        },
        {
            "alt": "Хол “название учереждения”",
            "src": "/static/gallery-slider/gallery-photo-14.webp"
        },
        {
            "alt": "Танцевальный зал",
            "src": "/static/gallery-slider/gallery-photo-15.webp"
        },
        {
            "alt": "Студия танцев",
            "src": "/static/gallery-slider/gallery-photo-16.webp"
        },
        {
            "alt": "Зал для танцев",
            "src": "/static/gallery-slider/gallery-photo-17.webp"
        },
        {
            "alt": "Тренажерный зал",
            "src": "/static/gallery-slider/gallery-photo-18.webp"
        },
        {
            "alt": "Секция боевых искусств",
            "src": "/static/gallery-slider/gallery-photo-19.webp"
        }
    ],
    "secondaryInfo": "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана! Давайте знакомится, посмотрите видео и если останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24 / 7 и ответить на все вопросы. Мы предложим спортивные, творческие и развивающие кружки и секции для детей и взрослых любого возраста. Количество мест может быть ограничено."
    }
    
}

module.exports = {
    section, 
    sections
}