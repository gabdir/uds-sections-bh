const fs = require("fs");
const path = require("path");

const router = require("express").Router();


router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://89.223.91.151:8080");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const loadJson = (filepath, encoding = "utf8") =>
  JSON.parse(
    fs.readFileSync(path.resolve(__dirname, `${filepath}.json`), { encoding })
  );

const waitMiddleWare = (req, res, next) => {
  setTimeout(next, 1000); 
}

router.use(waitMiddleWare)

router.get(`/section`,(req, res) => {
    res.send(loadJson('./data'));
})

router.get(`/sections` ,(req, res) => {
  res.send(loadJson('./sectionData'));
})



module.exports = router;
