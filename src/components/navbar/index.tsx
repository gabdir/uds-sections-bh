import React from 'react'
import { Navbar } from '@ijl/uds-ui'
import { getNavigations } from '@ijl/cli'

const navigations = getNavigations()

export default () => (
    <div style={{ margin: '21px 0 40px 0' }} data-cy="navbar">
        <Navbar
            main={navigations['main']}
            orgs={navigations['orgs']}
            news={navigations['news']}
            sections={navigations['sections']}
            rates={navigations['rates']}
            delay={250}
            active='sections'
        />
    </div>
)
