import React from 'react';
import {Typography} from '@ijl/uds-ui';

import {instagram, twitter, ok, facebook, vk} from '../../assets'

const Share = () => {
    return (
        <div>
            <div style={{justifyContent:"center"}}>
            <Typography component="h3" fontWeight={600} fontSize="38px" margin="0 0 30px 0">Мы в социальных сетях</Typography>
            </div>
            <div style={{justifyContent:"center"}}>
            <img src={vk} alt="icon" style={{marginRight:"48px"}}/>
            <img src={instagram} alt="icon" style={{marginRight:"48px"}}/>
            <img src={ok} alt="icon" style={{marginRight:"48px"}}/>
            <img src={facebook} alt="icon" style={{marginRight:"48px"}}/>
            <img src={twitter} alt="icon"/>
            </div>
        </div>
    )
};

export default Share;