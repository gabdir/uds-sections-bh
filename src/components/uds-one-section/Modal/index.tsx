import * as React from 'react';
import axios from 'axios'; 

import {StyledModal, CloseButton, Label, StyledSelect, FullText, LeftSection, RightSection} from './styled'

import {Typography, TextInput, PageLoader, Button} from '@ijl/uds-ui'
import {getConfig} from '@ijl/cli'

const baseUrl = getConfig()['sections.api.base.url']

type PopupData = {
  id: number; 
  section: string
}

interface ModalProps {
  showModal: boolean;
  toggleModal: () => void;
  setPopup: (string) => void;
  region?: PopupData[];
  institution?: PopupData[];
  sections?: PopupData[];
}



interface SelectProps {
  label: String;
  data: PopupData[];
  name?: string
}

const Select: React.FC<SelectProps> = ({ label, data, name }: SelectProps) => {
  if (!data?.length){
    return <div />
  }
  return (
    <div>
      <Label required>
        {label}
      </Label>
      <StyledSelect name={name}>
        {data.map((item) => {
        return (
          <option style={{width:'100%'}} key={item.id}>
            {item.section}
          </option>
        )
        })}
      </StyledSelect>
    </div>
  )
}

export const ModalInfo: React.FC<ModalProps> = (props) => {
  const [data, updateData] = React.useState(null);
  React.useEffect(()=> {
      axios.get(`${baseUrl}/sections`).then((answer)=> {
          updateData(answer.data.sections)
      })
      }, [])
  
  if (!data?.length){
    return <div></div>
  }

  const handleSuccess = (event) => {
    if (!event.target.checkValidity()){
      event.preventDefault(),
      props.toggleModal(),
      props.setPopup('fail')
      return
    }
    event.preventDefault(),
    props.toggleModal(),
    props.setPopup('success')
  }
  const handleFail = () => {
    props.toggleModal(),
    props.setPopup('fail')
  }

  const sections = []
  let id = 0
  data.map((group) => {group.sections.forEach((element) => {
    sections.push({
      id: id,
      section:element.name
    })
    id++
  }
  )
})
  const inputStyle = {
    marginBottom: 20
  }
  return (
    <StyledModal
      isOpen={props.showModal}
      onBackgroundClick={handleFail}
      onEscapeKeydown={handleFail}
    >
      <CloseButton onClick={props.toggleModal}>
          <img src="https://icon.now.sh/x" />
      </CloseButton>
      <LeftSection>
        <FullText><i>UDS</i></FullText>
      </LeftSection>
      <RightSection> 
        <form onSubmit={handleSuccess}>
          <Typography
            component={'h2'}
            fontWeight={600}
          >
            Запись
          </Typography>
          <Select 
          name="region"
          label="Округ или район" 
          data={props.region || sections} /> 
          <Select 
            name="institution"
            label="Учреждение" 
            data={props.institution || sections} />  
          <Select 
            name="sections"
            label="Секция" 
            data={props.sections || sections} />  
        
          <TextInput 
            name="name"
            style={inputStyle} label='Имя' required />
          <TextInput 
            name="surname"
            style={inputStyle} label='Фамилия' required/>
          <TextInput 
            name="tel"
            style={inputStyle} label='Телефон, по которому мы можем с вами связаться' type='tel' required/>
          <TextInput 
            name="email"
            style={inputStyle} label='Электронная почта'/>
          <TextInput 
            name="comment"
            style={inputStyle} label='Комментарий' />
          <Button
             type="submit"
            style={{margin: '15px 0'}} >Записаться</Button>
        </form>
      </RightSection> 
    </StyledModal>
  )
}