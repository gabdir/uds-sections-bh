import {Typography, Button} from '@ijl/uds-ui'
import {PopupModal, CloseButton} from './styled'
import  React, {useEffect, useState, useCallback} from 'react';

interface PopupProps {
  appear: boolean;
  setPopup: (string) => void;
  image: string;
  title: string;
  body: string;
}

export const Popup: React.FC<PopupProps> =(props)=> {

  const togglePopup = useCallback( 
    () => props.setPopup(''), []
  )

  return (
    <PopupModal
      isOpen={props.appear}
      onBackgroundClick={togglePopup}
      onEscapeKeydown={togglePopup}
    >
      <CloseButton onClick={togglePopup}>
          <img src="https://icon.now.sh/x" />
      </CloseButton>
      <img src={`${__webpack_public_path__}static/${props.image}`}/>
      <Typography 
        component='h2'
        fontSize='38px'
        fontWeight={600}
      >
          {props.title}
      </Typography>
      <Typography
        style={{textAlign: 'center'}}
        component='p'
        fontSize='16px'
      >
        {props.body}
      </Typography>
      <Button onClick={togglePopup}>Понятно</Button>
    </PopupModal>
  )
}