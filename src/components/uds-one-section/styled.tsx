import styled from 'styled-components'

export const Content = styled.div`
    max-width: 100%; 
`

export const Main = styled.div`
    margin-right: 132.5px; 
    margin-left: 132.5px; 
`

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center; 
`

export const ShareContent = styled.div`
    width: 512px; 
    height: 134px; 
`

export const GoalContent = styled.div`
    margin-bottom: 104px; 
    margin-top: 104px;

    padding: 0;

    @media (min-width: 810px) {
        padding: 0 8%;
        margin-bottom: 100px;
    }
`

export const SimilarContent = styled.div`
    width: 363px; 
    height: 202px;
    display: flex; 
    justify-content: flex-end;
`

export const BodyContent = styled.div`
    height: 398px; 
    margin-right: 149.5px; 
    margin-left: 149.5px; 
    display: flex; 
    flex-direction: row;
    margin-top: 104px; 
    margin-bottom: 104px;
`

export const SecondaryContent = styled.div`
    width: 656px; 
    height: 352px;
    display: flex; 
    justify-content: flex-start; 
`

export const Secondary = styled.div`
    height: 352px;
    display: flex;
    margin-right: 128px;
    margin-left: 128px;
    flex-direction: row;
    justify-content: space-between;
`

export const NavbarWrapper = styled.div`
    margin-bottom: 40px;
`

