import * as React from 'react';

import {
    NoImagePlaceHolder,
} from '../../../assets';
import {
    Wrapper,
    TitleSlider,
    ContentSlider,
    StyledArrow,
    SliderImage,
    StyledSwipeable
} from './styled';
import { Slider } from './slider';

/**
 * Информация о слайде.
 *
 * @prop {string} src Путь до картинки.
 * @prop {string} title Заголовок слайда.
 * @prop {string} alt Alt картинки.
 */
interface SlideInfo {
    src: string;
    alt: string;
}

// Компонент галерея.
export class GallarySlider extends Slider<SlideInfo> {
    /**
     * Рендерит картинку для блока картинок.
     *
     * @param {SlideInfo} slide Слайд.
     * @param {number} index Индекс слайда.
     */
    renderStyledImageBlock = (slide: SlideInfo, index: number) => {
        const order = this.getOrder(index);

        if (order > 4) {
            return null;
        }

        return (
            <SliderImage key={'slide-' + index} src={`${__webpack_public_path__}${slide.src}`} order={order} gap={20} alt={slide.alt} />
        );
    }

    // Рендерит центральную часть с картинкой.
    renderCenterPart = () => {
        const { array } = this.props;

        if (!array.length) {
            return (
                <NoImagePlaceHolder />
            );
        }

        return (
            <StyledSwipeable
                onSwipedLeft={this.handleLeftSwipe}
                onSwipedRight={this.handleSwipe}
            >
                {array.map(this.renderStyledImageBlock)}
            </StyledSwipeable>
        );
    }

    render() {
        const { array } = this.props;

        return (
            <Wrapper>
                <ContentSlider>
                    <StyledArrow data-cy="gallary-slider-arrow-left" onClick={this.prevSlide} size={32} />
                    {this.renderCenterPart()}
                    <StyledArrow data-cy="gallary-slider-arrow-right" onClick={this.nextSlide} flip={true} size={32} />
                </ContentSlider>
            </Wrapper>
        );
    }
}
