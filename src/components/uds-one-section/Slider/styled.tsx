import styled from 'styled-components';
import { Swipeable } from 'react-swipeable';

import { ArrowInSirlce } from '../../../assets/';
import { device } from '../../../__data__/constants';

const calcResponsive = (min, max) =>
    `calc(${min}px + (${max} - ${min}) * ((100vw - 768px) / (1440 - 768)))`;

export const Wrapper = styled.section`
    position: relative;
    font-family: Montserrat;
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.17;
    letter-spacing: normal;
    color: #fff;
    align-self: center;
    justify-self: center;
    max-width: 1400px;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-bottom: ${calcResponsive(45, 83)};
`;

export const TitleSlider = styled.div`
    position: relative;
    width: 100%;
    font-size: ${calcResponsive(35, 56)};
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    text-align: center;
    color: #1e1d20;
    margin-bottom: ${calcResponsive(20, 40)};

    @media (min-width: 1440px) {
        font-size: 56px;
    }
`;

export const ContentSlider = styled.div`
    position: relative;
    width: 100%;
    overflow: hidden;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const StyledArrow = styled(ArrowInSirlce)`
    width: 5%;
    z-index: 1;
    padding-left: 9%;
    opacity: .2;
    opacity: 0;

    @media ${device.tablet} {
        opacity: 1;
        width: 15%;
        padding-left: 9%;
    }

    :hover {
      opacity: 1;
      cursor: pointer;
    }
`;

export const CenterImg = styled.div`
    max-height: 631px;
    position: relative;
    width: 70%;
    height: 100%;
    overflow: hidden;
    display: grid;
`;

export const StyledImageBlock: any = styled.div`
    flex: 1 0 100%;
    flex-basis: 80%;
    margin-right: 20px;
    order: ${(props: any) => props.order};
`;

/**
 * @prop {number} order Порядковый номер в очереди картинок.
 * @prop {number} gap Расстояние (в процентах) между слайдами.
 */
interface SliderImageProp {
    order: number;
    gap: number;
}

export const SliderImage = styled.img`
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    opacity: ${(props: SliderImageProp) => props.order === 2 ? 1 : .3};
    order: ${(props: any) => props.order};
    transition: left 1s ease-in-out, opacity 1s ease-in-out;
    left: ${(props: SliderImageProp) => (props.order * (100 + props.gap / 3)) - (100 + props.gap / 3) * 2}%;
    display: ${(props: SliderImageProp) => props.order <= 4 ? 'block' : 'none'};

    @media ${device.tablet} {
        left: ${(props: SliderImageProp) => (props.order * (100 + props.gap)) - (100 + props.gap) * 2}%;
    }
`;

export const StyledSwipeable = styled(Swipeable)`
    height: 230px;
    width: 100%;
    position: relative;
    background-color: #fff;

    @media ${device.tablet} {
        height: ${calcResponsive(400, 550)};
    }

    @media (min-width: 1440px) {
        height: 550;
    }
`;
