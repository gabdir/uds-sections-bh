import * as React from 'react';
import { throttle } from 'lodash';

interface SliderProps<T> {
    array: Array<T>;
}

export class Slider<S> extends React.Component<SliderProps<S>> {
  constructor(props){
    super(props)
  }
  state = { 
    selected: 0,
    disableLeft: true,
    disableRight: false,
    marginLeft: 0,
    position: 0,
    direction: 'next',
    sliding: false
  };

  doSliding = (direction: string, position: number) => {
    this.setState({
      sliding: true,
      direction,
      position
    });

    setTimeout(
      () => {
        this.setState({
          sliding: false
        });
      },
      1000
    );
  }

  /**
   * Переключает текущий слайд на следующий.
   */
  nextSlide = () => {
    const { position, sliding } = this.state;
    const { array } = this.props;

    if (!sliding) {
      const numItems = array.length || 1;
      this.doSliding('next', position === numItems - 1 ? 0 : position + 1);
    }
  }
  prevSlide = () => {
    const { position, sliding } = this.state;
    const { array } = this.props;

    if (!sliding) {
      const numItems = array.length;
      this.doSliding('prev', position === 0 ? numItems - 1 : position - 1);
    }
  }
  handleSwipe = throttle(
    (isNext: boolean = false) => {
      if (isNext === true) {
        this.nextSlide();
      } else {
        this.prevSlide();
      }
    },
    500,
    { trailing: false }
  );

  handleLeftSwipe = () => this.handleSwipe(true);

  getOrder(itemIndex: number) {
    const { position } = this.state;
    const { array } = this.props;
    const numItems = array.length || 1;

    if ( numItems === 1) {
        return 1;
    }

    if ( numItems === 2) {
      itemIndex = itemIndex + 1;
    }

    if (itemIndex - position < 0) {
      return numItems - Math.abs(itemIndex - position);
    }

    return itemIndex - position;
  }
  render() {
    return (
      <div></div>
    )
  }
}