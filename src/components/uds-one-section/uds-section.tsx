import React, {useState} from 'react';
import {ModalProvider} from 'styled-react-modal'
import {OrganizationInfo} from '@ijl/uds-ui'
import {getConfig} from '@ijl/cli'

import {ModalInfo} from './Modal/index'
import {Popup} from './Modal/popup'

interface Props {
    title: string; 
    subTitle: string; 
    image: string; 
};

export const UdsSection: React.FC<Props> = ({title, subTitle, image}) =>  {
    const [isOpen, setIsOpen] = useState(false);
    const [popup, setPopup] = useState('');

    const toggleModal: any = (e) => {
        setIsOpen(!isOpen)
    }
    
    const popupToShow: any = (type: string) => {
        setPopup(type)
    }

    return (
        <div>
            <OrganizationInfo bottomText=""
            buttonText="Записаться"
            mainText={subTitle}
            src={`${__webpack_public_path__}static/${image}`}
            title={title}
            onClick={toggleModal}/>
            <ModalProvider>
                <ModalInfo 
                showModal={isOpen} 
                toggleModal={toggleModal} 
                setPopup={popupToShow}
                />

                <Popup 
                appear={ popup === 'success' }
                image={'ico-success.svg'}
                title={'Здорово!'}
                body={'Наш менеджер свяжется с вами по указанному телефону'} 
                setPopup={popupToShow} />

                <Popup 
                appear={ popup === 'fail' }
                image={'ico-unsuccess.svg'}
                title={'Ой!'}
                body={`Что-то пошло не так. Повторите попытку позже.`} 
                setPopup={popupToShow} />
            </ModalProvider>
        </div>
    );
};