import React,{Component} from 'react';
import styled from 'styled-components';

import {Typography} from '@ijl/uds-ui'
import ColoredLine from '../ColoredLine'

interface Props {
}

const SectionsWrapper = styled.div`
    width: 363px; 
    height: 202px;
`

export default class extends Component<Props> {
    render() {
        return (
            <div data-cy="similar-sections">
                <SectionsWrapper>
                    <div style={{marginBottom: '16px'}}>
                    <Typography component="h3" fontWeight={600} fontSize="22px">Похожие секции</Typography>
                    </div>
                    <ColoredLine color="#000000"/>
                        <Section name="Футбол"/>
                        <ColoredLine color="#000000"/>
                        <Section name="Хоккей"/>
                        <ColoredLine color="#000000"/>
                        <Section name="Воллейбол"/>
                        <ColoredLine color="#000000"/>
                </SectionsWrapper>
            </div>

        );
    }
};

const Section = ({name}) => {
    return (
        <div style={{marginBottom:'16px', marginTop:'16px'}}>
            <Typography component="a" style={{textDecoration: "none"}} href={"#"} fontSize="16px" color="#003EFF" fontWeight={700}>{name}</Typography>
        </div>
    );
};