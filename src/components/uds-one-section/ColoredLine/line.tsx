import React from 'react';

export const ColoredLine = ({ color }) => (
    <hr
        style={{
            borderTop: color
        }}
    />
);