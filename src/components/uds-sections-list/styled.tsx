import styled from 'styled-components';

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const Content = styled.div`
    max-width: 100%; 
`
export const SectionContent = styled.div`
    margin-right: 148px;
    margin-left: 148px;
`

export const SectionWrapper = styled.div`
    width: 100%; 
    height: auto;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap; 
`
export const AlphabetWrapper = styled.div`
    width: 100%;
    height: 615px;
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
`

export const NavbarWrapper = styled.div`
    margin-bottom: 40px;
`