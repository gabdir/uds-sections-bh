import React from 'react';
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect';
import {getSections} from '../../__data__/selectors/sections-selector'

import {Typography} from '@ijl/uds-ui'
import {ContentWrapper, Content, SectionWrapper, AlphabetWrapper, NavbarWrapper, SectionContent} from './styled'
import ColoredLine from '../uds-one-section/ColoredLine'

const Sort = (arr) => {

}

const UdsList = ({sections}) => {
    const alphabet = []
    sections.map((group) => {group.sections.forEach(element => alphabet.push({name:element.name, id:element.id}))})
    alphabet.sort(function (a, b) {
        return a.value - b.value;
    });

    // sort by name
    alphabet.sort(function(a, b) {
        var nameA = a.name.toUpperCase(); // ignore upper and lowercase
        var nameB = b.name.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    });


   return(
        <div>
            <ContentWrapper>
            <Content>
            <Typography component="h2" fontSize="50px"
                fontWeight={600}>
                Секции в Москве
            </Typography>
            <Typography component="h3" fontSize="38px"
                fontWeight={600} margin="48px 0 0 0">
                По группам
            </Typography>
            <ColoredLine color="#000000"/>
            {sections.map((group)=> (
                <div key={group.id}>
                    <Typography component="h3" fontSize="22px"
                    fontWeight={600}>
                        {group.name}
                    </Typography>
                        <ul>
                        <SectionWrapper key={group.id} data-cy="sectionId">
                            {group.sections.map((section) =>(
                                <div style={{width:'25%'}} key={section.id}>
                                    <li key={section.id} style={{ listStyleType: "none", margin: '0.5em'}}>
                                        <Typography component="a" style={ {textDecoration: "none"}} href={"sections/1"} fontSize="16px" fontWeight={400} margin="0 0 0 0">{section.name}</Typography>
                                    </li>
                                </div>
                            ))}
                        </SectionWrapper>
                        </ul>
                    
                </div>
            ))}
            <Typography component="h3" fontSize="38px"
                        fontWeight={600} margin="48px 0 0 0">
                По алфавиту
            </Typography>
            <ColoredLine color="#000000"/>
            <AlphabetWrapper>
                {alphabet.map((section) =>(
                    <div style={{width:'25%'}} key={section.id}>
                            <li style={{ listStyleType: "none", margin: '0.5em' }} key={section.id}>
                                <Typography component="a" style={ {textDecoration: "none"}} href={"sections/1"} fontSize="16px" fontWeight={400} margin="0 0 0 0">{section.name}</Typography>
                                 </li>
                    </div>
                ))}
            </AlphabetWrapper>
            </Content>
            </ContentWrapper>
        </div>
   )
}

const mapStateToProps = () => createStructuredSelector({
    sections: getSections,
})

export default connect(mapStateToProps)(UdsList)