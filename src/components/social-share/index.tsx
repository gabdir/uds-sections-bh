import React from 'react'
import { SocialShare } from '@ijl/uds-ui'

export default function index() {
    return (
        <div data-cy='social-link'>
            <SocialShare title="Мы в социальных сетях"
                    alt="Мы в соцсетях"
                    vk_link={'https://vk.com'}
                    instagram_link={'https://instagram.com'}
                    facebook_link={'https://facebook.com'}
                    ok_link={'https://ok.ru'}
                    twitter_link={'https://twitter.com'}></SocialShare>
        </div>
    )
}
