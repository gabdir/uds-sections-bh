import { createSelector } from 'reselect';

const getStore = state => state;
const getsectionData = createSelector(getStore, (state) => state?.sectionReducer?.data);

export const getSectionDataLoading = createSelector(getStore, (state) => state?.sectionReducer?.loading);
export const getError = createSelector(getStore, (state) => state?.sectionReducer?.error);

export const getImgArr = createSelector(getsectionData, (data) => data?.imgArr);
export const getSectionTitle = createSelector(
    getsectionData,
    (data) => data?.title || ''
)

export const getSectionSubtitle = createSelector(
    getsectionData,
    (data) => data?.subTitle || ''
)

export const getBodyContent = createSelector(
    getsectionData, 
    (data) => data?.body || ''
)

export const getSecondaryInfo = createSelector(
    getsectionData, 
    (data) => data?.secondaryInfo || ''
)

export const getImg = createSelector(
    getsectionData, 
    (data) => data?.image || ''
)

export const getSectionLinks = createSelector(
    getsectionData,
    (data) => data?.links || {}
)

export const getBodyImg = createSelector(
    getsectionData, 
    (data) => data?.body.image || ''
)

export const getBodyTitle = createSelector(
    getsectionData, 
    (data) => data?.body.title || ''
)