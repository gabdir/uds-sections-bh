import { createSelector } from 'reselect';

const getStore = state => state;

const getsectionsData = createSelector(getStore, (state) => state?.sectionsReducer?.data);

export const getSectionsDataLoading = createSelector(getStore, (state) => state?.sectionsReducer?.loading);
export const getError = createSelector(getStore, (state) => state?.sectionsReducer?.error);

export const getSections = createSelector(
    getsectionsData,
    (data) => data ? data : ''
)


