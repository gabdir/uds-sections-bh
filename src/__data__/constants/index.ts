/**
 * Размеры переходных точек.
 *
 * @prop {string} key Ключ размера.
 */
interface Size {
    [key: string]: string;
  }
  
  const size: Size = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '810px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px'
  };
  /**
   * Медиа запросы.
   *
   * @prop {string} key Ключ размера.
   */
  interface Device {
    [key: string]: string;
  }
  
  const device: Device = {
  mobileS: `(min-width: ${size.mobileS})`,
  mobileM: `(min-width: ${size.mobileM})`,
  mobileL: `(min-width: ${size.mobileL})`,
  tablet: `(min-width: ${size.tablet})`,
  laptop: `(min-width: ${size.laptop})`,
  laptopL: `(min-width: ${size.laptopL})`,
  desktop: `(min-width: ${size.desktop})`,
  desktopL: `(min-width: ${size.desktop})`
  };
  
  /**
   * Ccылки на соц сети.
   *
   * @prop {string} vk Ссылка на vk.
   * @prop {string} instagram Ссылка на instagram.
   * @prop {string} ok Ссылка на ok.
   * @prop {string} facebook Ссылка на facebook.
   * @prop {string} twitter Ссылка на twitter.
   */
  interface SocialLinks {
      vk: string;
      instagram: string;
      ok: string;
      facebook: string;
      twitter: string;
  }
  
  const socialLinks: SocialLinks = {
    vk: 'http://vk.com/uds_moscow',
    instagram: 'https://www.instagram.com/uds_moscow/',
    ok: 'https://www.ok.ru/group/54615427383430/',
    facebook: 'https://www.facebook.com/uds.moscow',
    twitter: 'https://twitter.com/uds__moscow',
  };
  
  export {size, device, socialLinks};