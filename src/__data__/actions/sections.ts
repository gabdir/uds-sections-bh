import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { getConfig } from '@ijl/cli'
import {SectionsDataResponse} from '../model/interface'

import {
    SECTIONS_DATA_FETCH,
    SECTIONS_DATA_FETCH_FAIL,
    SECTIONS_DATA_FETCH_SUCCESS
} from '../constants/uds-constants'


const api = getConfig()['sections.api.base.url']


const getFetchAction = () => ({
    type: SECTIONS_DATA_FETCH,
})

const getSuccessAction = (data) => ({
    type: SECTIONS_DATA_FETCH_SUCCESS,
    data
})

const getErrorAction = () => ({
    type: SECTIONS_DATA_FETCH_FAIL,
})


export default () => async (dispatch: any) => {
    dispatch(getFetchAction())

    const requestProps: AxiosRequestConfig = {
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
        }
    };

    try {
        const answer: AxiosResponse<SectionsDataResponse> = await axios(`${api}/sections`, requestProps);
        if (answer.data?.status?.code === 0) {
            dispatch(getSuccessAction(answer.data.sections));
        } else {
            dispatch(getErrorAction());
        }

    } catch (error) {
        dispatch(getErrorAction());
    }
}