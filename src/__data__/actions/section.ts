import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { getConfig } from '@ijl/cli'

import {
    SECTION_DATA_FETCH,
    SECTION_DATA_FETCH_SUCCESS,
    SECTION_DATA_FETCH_FAIL
} from '../constants/uds-constants'

const api = getConfig()['sections.api.base.url']

const getFetchAction = () => ({
    type: SECTION_DATA_FETCH,
})

const getSuccessAction = (data) => ({
    type: SECTION_DATA_FETCH_SUCCESS,
    data
})

const getErrorAction = () => ({
    type: SECTION_DATA_FETCH_FAIL,
})


export default (id: number) => async (dispatch: any) => {
    dispatch(getFetchAction())

    const requestProps: AxiosRequestConfig = {
        method: 'get',
        params: {
            id: id
        },
        headers: {
            'Content-Type': 'application/json',
        }
    };

    try {
        const answer: AxiosResponse = await axios(`${api}/section`, requestProps);

        if (answer.data?.status?.code === 0) {
            dispatch(getSuccessAction(answer.data.section));
        } else {
            dispatch(getErrorAction());
        }

    } catch (error) {
        dispatch(getErrorAction());
    }
}

