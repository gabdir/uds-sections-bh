import {combineReducers} from 'redux';
import {sectionsReducer} from './sections'
import {sectionReducer} from './section'

const rootReducer = combineReducers({
    sectionReducer,
    sectionsReducer
});

export default rootReducer;