import {
    SECTIONS_DATA_FETCH,
    SECTIONS_DATA_FETCH_FAIL,
    SECTIONS_DATA_FETCH_SUCCESS
} from '../constants/uds-constants'

const initialState = {
    data: null,
    loading: false,
    error: null
}

const fetchHandler = (state, action) => ({
    ...state,
    loading: true
})

const fetchSuccessHandler = (state, action) => ({
    ...state,
    loading: false,
    data: action.data,
})

const fetchFailHandler = (state, action) => ({
    ...state,
    loading: false,
    error: action.error || true,
    data: null
})

const handlers = {
    [SECTIONS_DATA_FETCH]: fetchHandler,
    [SECTIONS_DATA_FETCH_SUCCESS]: fetchSuccessHandler,
    [SECTIONS_DATA_FETCH_FAIL]: fetchFailHandler,
}

export const sectionsReducer = (state = initialState, action) =>
    Object.prototype.hasOwnProperty.call(handlers, action.type) ? handlers[action.type](state, action) : state;