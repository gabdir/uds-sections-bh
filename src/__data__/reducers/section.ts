import {
    SECTION_DATA_FETCH,
    SECTION_DATA_FETCH_FAIL,
    SECTION_DATA_FETCH_SUCCESS
} from '../constants/uds-constants'

const initialState = {
    data: null,
    loading: false,
    error: null
}

const fetchHandler = (state, action) => ({
    ...state,
    loading: true
})

const fetchSuccessHandler = (state, action) => ({
    ...state,
    loading: false,
    data: action.data,
})

const fetchFailHandler = (state, action) => ({
    ...state,
    loading: false,
    error: action.error || true,
    data: null
})

const handlers = {
    [SECTION_DATA_FETCH]: fetchHandler,
    [SECTION_DATA_FETCH_SUCCESS]: fetchSuccessHandler,
    [SECTION_DATA_FETCH_FAIL]: fetchFailHandler,
}

export const sectionReducer = (state = initialState, action) =>
    Object.prototype.hasOwnProperty.call(handlers, action.type) ? handlers[action.type](state, action) : state;