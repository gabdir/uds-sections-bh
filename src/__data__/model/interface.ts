export interface SectionsDataResponse {
    status: Status;
    sections: Category[];
}

export interface Status {
    code: number;
}

export interface Category {
    [category: string]: string[];
}
