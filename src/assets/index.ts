import {ArrowInSirlce,NoImagePlaceHolder} from './svgIcons';
import facebook from './icons/social/facebook.svg';
import instagram from './icons/social/instagram.svg';
import ok from './icons/social/odnoklassniki.svg';
import twitter from './icons/social/twitter.svg';
import vk from './icons/social/vk.svg';
const social = {
    icons: {
        facebook,
        instagram,
        ok,
        twitter,
    },
}

export {
    instagram, 
    twitter, 
    ok, 
    facebook, 
    vk, 
    ArrowInSirlce, 
    NoImagePlaceHolder
};
