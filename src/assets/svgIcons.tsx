import * as React from 'react';
import styled from 'styled-components';

const Icon: any = styled.svg`
  ${(props: any) => (props.flip ? 'transform: rotate(180deg);' : '')}
`;

/**
 * @prop {boolean} [flip] Признак поворота на 180 градусов.
 * @prop {number} [size] Размер отвечающий за ширину и высоту в px.
 * @prop {boolean} [disable] Признак заблокированного состояния.
 */
interface ArrowProps extends React.SVGAttributes<HTMLOrSVGElement> {
  flip?: boolean;
  size?: number;
  disable?: boolean;
}

// tslint:disable:max-line-length jsx-alignment
export const ArrowInSirlce = ({ flip, size, disable, ...rest }: ArrowProps) => (
  <Icon
    flip={flip}
    width={size || 64}
    height={size || 64}
    viewBox="0 0 64 64"
    {...rest}
  >
    <path
      fill={(disable && '#979797') || '#003eff'}
      d="M0 32C0 14.35 14.35 0 32 0s32 14.35 32 32-14.35 32-32 32S0 49.65 0 32z"
    />
    <path
      fill="#fff"
      d="M22.674 33.219A2.352 2.352 0 0 1 22 31.602c0-.606.27-1.213.674-1.617l12.328-12.328c.876-.876 2.358-.876 3.234 0 .876.876.876 2.358 0 3.234L27.592 31.602l10.644 10.644a2.19 2.19 0 0 1 .673 1.617c0 .606-.202 1.213-.673 1.617-.876.876-2.358.876-3.234 0L22.674 33.219z"
    />
  </Icon>
);


export const NoImagePlaceHolder = (props: any) => (
  <Icon flip={props.flip}>
    <path
      d="M3,248.5 L253,248.5"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M253.215043,248.5 L253.215043,76"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M2.13186747,248 L2.00108221,78.0000503"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M32.1318675,172.5 L32.1318675,77"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M32.1318675,172.5 L222.5,172.5"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M61.499182,2.50127822 L193,2.50127822"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M222.5,172.5 L223,75.5"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M61.499182,42.5 L193,42.5"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <circle stroke="#979797" fill="#979797" cx="83.5" cy="83.5" r="11.5" />
    <circle stroke="#979797" fill="#979797" cx="172.5" cy="83.5" r="11.5" />
    <path
      d="M83.5,132.5 C84.970505,129.982977 86.3296208,128.504521 87.5773476,128.064631 C100.333857,123.567284 114.823993,123.629262 128,123.629262 C141.215143,123.629262 154.441266,123.540528 167.234415,128.064631 C168.456763,128.496895 170.211958,129.975352 172.5,132.5"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
      fill="none"
    />
    <path
      d="M2.13186747,77 L61.499182,76.1866883"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M193,75.5 L253,75.5"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M193,75.5 L193,2.50127822"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M61.499182,76.1866883 L61.499182,2.5"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M3,75 L60,3"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M252,73 L194,3"
      stroke="#979797"
      strokeWidth="4"
      strokeLinecap="square"
    />
    <path
      d="M184,123.420419 C187.865993,123.420419 191,119.466577 191,114.589266 C191,109.711954 185.75,96.0127115 184,98.243589 C182.25,100.474467 177,109.711954 177,114.589266 C177,119.466577 180.134007,123.420419 184,123.420419 Z"
      stroke="#4A90E2"
      fill="#4A90E2"
    />
    <text fontSize="32" fontWeight="normal" fill="#878585">
      <tspan x="48" y="221">
        NO IMAGE
      </tspan>
    </text>
  </Icon>
);
