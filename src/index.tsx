import React from 'react';
import ReactDOM from 'react-dom';

import App from './App'; 

export default () => <App/>;

export const mount = (Сomponent) => {
    ReactDOM.render(
        <Сomponent/>,
        document.getElementById('app')
    );
};


export const unmount = () => {
    ReactDOM.unmountComponentAtNode(document.getElementById('app'))
};