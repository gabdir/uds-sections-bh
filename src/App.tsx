import React from 'react';
import { Provider} from "react-redux";
import { createStore, applyMiddleware } from 'redux'
import {Route, Switch,} from 'react-router';
import thunk from 'redux-thunk'
import {Router} from 'react-router-dom';
import { composeWithDevTools } from 'redux-devtools-extension';
import {createBrowserHistory} from 'history';

import Sections from './containers/Sections'
import SectionPage from './containers/SectionPage'
import reducers from '../src/__data__/reducers'

const history = createBrowserHistory()

export default class App extends React.Component{
    render() {
        return (
            <Provider store={createStore(reducers, composeWithDevTools(applyMiddleware(thunk)))}>
            <Router history={history}>
                <Switch>
                    <Route path="/sections" exact component={Sections}/>
                    <Route path="/sections/:id" component={SectionPage}/>
                </Switch>
            </Router>
            </Provider>
        );
    }
}