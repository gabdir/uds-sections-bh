import React, {useEffect} from 'react';
import { bindActionCreators } from 'redux';
import {connect} from "react-redux";
import {PageLoader, Error, Typography} from '@ijl/uds-ui'
import { createStructuredSelector } from 'reselect';
import { getNavigations } from '@ijl/cli'

import fetchSectionsData from '../__data__/actions/sections'
import { getSectionsDataLoading, getError, getSections } from '../__data__/selectors/sections-selector'
import Navbar from '../components/navbar'
import Footer from '../components/footer'
import UdsList from '../components/uds-sections-list'
import {Wrapper} from './styled'


const Sections = ({fetchSectionsData, isLoading,sections,isError}) => {
    useEffect(()=>{fetchSectionsData();},[]);

    if (isError) {
        return <Error type='error' link={getNavigations()['main']} linkText='Перейти на главную' />
    }

    if (isLoading || !sections) {
        return <PageLoader />
    }
    let content: JSX.Element;
    content = <UdsList/>

    return (
        <>
            <Wrapper>
                <Navbar />
                {content}
            </Wrapper >
            <Footer />
        </>

    )
}

const mapStateToProps = (state) => createStructuredSelector({
    isError: getError,
    isLoading: getSectionsDataLoading,
    sections: getSections,
})

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    fetchSectionsData
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Sections)
