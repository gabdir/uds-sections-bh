import styled from '@ijl/uds-ui/node_modules/styled-components'

export const titleStyles = `
    font-size: 22px;
    margin: 0 0 24px 0;

    @media (min-width: 810px) {
        font-size: 50px;
        margin: 0 0 40px 0 ;
    }
`

export const Wrapper = styled.div`
    padding: 0 8%;
    background: #FFFFFF;
    margin-bottom: 44px;

    @media (min-width: 810px) {
        margin-bottom: 84px;
    }
`

export const SocialShareWrapper = styled.div`
    margin-bottom: 64px;

    @media (min-width: 810px) {
        margin-bottom: 104px;
    }
`

export const ReachGoalsWrapper = styled.div`
    padding: 0;
    margin-bottom: 0;

    @media (min-width: 810px) {
        padding: 0 8%;
        margin-bottom: 100px;
    }
`

export const Secondary = styled.div`
    margin-top: 104px; 
    margin-bottom: 104px;
	display: flex;
	flex-direction: row;
`

export const SecondaryContent = styled.div`
    display: flex; 
    justify-content: flex-start; 
`