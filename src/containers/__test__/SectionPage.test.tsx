import React from 'react';
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import moxios from 'moxios'
import { act } from 'react-dom/test-utils'
import { Router } from 'react-router-dom'
import renderer from 'react-test-renderer';
import { createBrowserHistory } from 'history'
import Modal from 'styled-react-modal';


import SectionPage from '../SectionPage'
import {GallarySlider} from '../../components/uds-one-section/Slider'
import {Slider} from '../../components/uds-one-section/Slider/slider'
import {imgArr, udsSection} from './imgArr'
import {UdsSection} from '../../components/uds-one-section/uds-section'
import {ModalInfo} from '../../components/uds-one-section/Modal/index'
import {store} from '../../__data__/store'

configure({ adapter: new Adapter() })

const history = createBrowserHistory()

describe('mount section page app', () => {
    beforeEach(() => {
      moxios.install();
      // @ts-ignore
      window.__webpack_public_path__ = '/'
    });
    afterEach(() => moxios.uninstall());

    it('mount section info', async () => {
        const app = mount(
            <Provider store={store}>
                <Router history={history}>
                  <SectionPage />
                </Router>
            </Provider>
        );
    
    expect(app).toMatchSnapshot();

    await moxios.wait(jest.fn);

    await act(async () => {
      const request = moxios.requests.mostRecent();
      await request.respondWith({
        status: 200,
        response: require('../../../stubs/api/data.json')
      })
    });
    app.update();
    expect(app).toMatchSnapshot();
  });

  it('mount error on error', async () => {
    const app = mount(
        <Provider store={store}>
            <Router history={history}>
                <SectionPage />
            </Router>
        </Provider>
    )

    expect(app).toMatchSnapshot()

    await moxios.wait(jest.fn)

    await act(async () => {
        const request = moxios.requests.mostRecent()
        await request.respondWith({
            status: 404,
        })
    })

    app.update()
    expect(app).toMatchSnapshot()
  })

  it('mount gallary slider', async () => {
      const app = renderer.create(
          <GallarySlider array={imgArr}/>
      );
  
      expect(app.toJSON()).toMatchSnapshot();
  });
  it('render slider', () => {
    const wrapper = mount(
      <Slider array={imgArr}/>
    );
    expect(wrapper.find('div')).toHaveLength(1);
    expect(new Slider(imgArr)).toBeInstanceOf(Slider)
  })



  it('mount uds-section', async () => {
    const app = mount(
        <Provider store={store}>
            <Router history={history}>
              <UdsSection title={udsSection.title} subTitle={udsSection.subTitle} image={udsSection.image}/>
            </Router>
        </Provider>
    );

    expect(app).toMatchSnapshot();
  });
});
