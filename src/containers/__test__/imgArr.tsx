interface SlideInfo {
    src: string;
    title: string;
    alt: string;
}
export const imgArr = [
    {
        "alt": "Тренажёрный зал",
        "src": "/static/gallery-slider/gallery-photo-1.webp"
    },
    {
        "alt": "Кисти для рисования",
        "src": "/static/gallery-slider/gallery-photo-2.webp"
    },
    {
        "alt": "Тренажерный зал",
        "src": "/static/gallery-slider/gallery-photo-3.webp"
    },
    {
        "alt": "Кабинет детской студии",
        "src": "/static/gallery-slider/gallery-photo-4.webp"
    },
    {
        "alt": "Комната детского досуга",
        "src": "/static/gallery-slider/gallery-photo-5.webp"
    },
    {
        "alt": "Кабинет для детского творчества",
        "src": "/static/gallery-slider/gallery-photo-6.webp"
    },
    {
        "alt": "Зал для танцев",
        "src": "/static/gallery-slider/gallery-photo-7.webp"
    },
    {
        "alt": "Детская студия",
        "src": "/static/gallery-slider/gallery-photo-8.webp"
    },
    {
        "alt": "Комната для занятий с детьми",
        "src": "/static/gallery-slider/gallery-photo-9.webp"
    },
    {
        "alt": "Кабинет военной подготовки",
        "src": "/static/gallery-slider/gallery-photo-10.webp"
    },
    {
        "alt": "Игровая комната",
        "src": "/static/gallery-slider/gallery-photo-11.webp"
    },
    {
        "alt": "Компьютерный класс",
        "src": "/static/gallery-slider/gallery-photo-12.webp"
    },
    {
        "alt": "Студия рисования",
        "src": "/static/gallery-slider/gallery-photo-13.webp"
    },
    {
        "alt": "Хол “название учереждения”",
        "src": "/static/gallery-slider/gallery-photo-14.webp"
    },
    {
        "alt": "Танцевальный зал",
        "src": "/static/gallery-slider/gallery-photo-15.webp"
    },
    {
        "alt": "Студия танцев",
        "src": "/static/gallery-slider/gallery-photo-16.webp"
    },
    {
        "alt": "Зал для танцев",
        "src": "/static/gallery-slider/gallery-photo-17.webp"
    },
    {
        "alt": "Тренажерный зал",
        "src": "/static/gallery-slider/gallery-photo-18.webp"
    },
    {
        "alt": "Секция боевых искусств",
        "src": "/static/gallery-slider/gallery-photo-19.webp"
    }
]

export const udsSection = {
        title: 'Мини-Футбол', 
        subTitle: 'С 2006 года базовый центр района по организации досуга и спортивных занятий для жителей. Наш Центр является многофункциональным и многопрофильным досуговым учреждением города Москвы: мы поддерживаем и развиваем все виды творчества, досуговой деятельности и любительского спорта. У Центра есть целых 9 отделений, поэтому до нас удобно добираться любому жителю Южного Бутова.', 
        image: 'Group2.svg'
}