import React from 'react';
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';

import {ArrowInSirlce, NoImagePlaceHolder} from '../../assets/svgIcons'

configure({ adapter: new Adapter() })

describe('mount icons', () => {
    it('mount arrow', async () => {
        const app = renderer.create(
            <ArrowInSirlce></ArrowInSirlce>
        );
    
        expect(app.toJSON()).toMatchSnapshot();
  });
  it('mount image', async () => {
    const app = renderer.create(
        <NoImagePlaceHolder></NoImagePlaceHolder>
    );

    expect(app.toJSON()).toMatchSnapshot();
});
})