import React from 'react';
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import moxios from 'moxios'
import { act } from 'react-dom/test-utils'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'

import Sections from '../Sections'
import {store} from '../../__data__/store'    

configure({ adapter: new Adapter() })

const history = createBrowserHistory()

describe('mount sections app', () => {
    beforeEach(() => {
      moxios.install();
      // @ts-ignore
      window.__webpack_public_path__ = '/'
    });
    afterEach(() => moxios.uninstall());

    it('mount section list', async () => {
        const app = mount(
            <Provider store={store}>
                <Router history={history}>
                  <Sections/>
                </Router>
            </Provider>
        );
    
    expect(app).toMatchSnapshot();

    await moxios.wait(jest.fn);

    await act(async () => {
      const request = moxios.requests.mostRecent();
      await request.respondWith({
        status: 200,
        response: require('../../../stubs/api/sectionData.json')
      })
    });
    app.update();
    expect(app).toMatchSnapshot();
  });

  it('mount error on error', async () => {
    const app = mount(
        <Provider store={store}>
            <Router history={history}>
                <Sections />
            </Router>
        </Provider>
    )

    expect(app).toMatchSnapshot()

    await moxios.wait(jest.fn)

    await act(async () => {
        const request = moxios.requests.mostRecent()
        await request.respondWith({
            status: 404,
        })
    })

    app.update()
    expect(app).toMatchSnapshot()
    })
});