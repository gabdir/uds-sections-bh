import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { useParams } from 'react-router-dom'
import { createStructuredSelector } from 'reselect'
import { ModalProvider } from 'styled-react-modal'

import { PageLoader, ReachGoals, Error, SocialShare, Typography } from '@ijl/uds-ui'
import { getNavigations } from '@ijl/cli'

import { ModalInfo } from '../components/uds-one-section/Modal/index'
import { Popup } from '../components/uds-one-section/Modal/popup'
import Navbar from '../components/navbar'
import Footer from '../components/footer'
import {UdsSection} from '../components/uds-one-section/uds-section'
import {GallarySlider} from '../components/uds-one-section/Slider'
import SimilarSections from '../components/uds-one-section/SimilarSections'
import fetchSectionData from '../__data__/actions/section'
import {Secondary, Wrapper, SocialShareWrapper, ReachGoalsWrapper, SecondaryContent} from './styled'

import { getSectionTitle, getBodyContent, getSectionSubtitle, getSectionDataLoading, getError, getImgArr, getSecondaryInfo, getImg, getSectionLinks, getBodyImg, getBodyTitle} from '../__data__/selectors/section-selector'

interface SlideInfo {
    src: string;
    alt: string;
}

type SectionProps = {
    fetchSectionData: Function;
    title: string;
    bodyContent: string;
    image: string; 
    subTitle: string;
    secondaryInfo: string; 
    imgArr: SlideInfo[];
    bodyImg: string; 
    bodyTitle: string;
    isLoading: boolean;
    isError: boolean;
    links: {
        vk: string;
        instagram: string;
        facebook: string;
        ok: string;
        twitter: string;
    };
}

const SectionPage: React.FC<SectionProps> = ({fetchSectionData, title, bodyContent, image, subTitle, secondaryInfo, imgArr, bodyImg, bodyTitle, isLoading, isError, links}) => {
    const { id } = useParams();

    const [isOpen, setIsOpen] = useState(false);
    const [popup, setPopup] = useState('');

    const toggleModal: any = (e) => {
        setIsOpen(!isOpen)
    }
    
    const popupToShow: any = (type: string) => {
        setPopup(type)
    }

    useEffect(() => {
        fetchSectionData(id)
    }, [])

    if (isError) {
        return <Error type='error' link={getNavigations()['main']} linkText='Перейти на главную' />
    }

    if (isLoading || !title || !bodyContent || !subTitle || !secondaryInfo || !links) {
        return <PageLoader />
    }

    return (
        <>
            <Wrapper>
                <Navbar />
                <UdsSection title={title} subTitle={subTitle} image={image}/>
                <Secondary>
                    <img style={{marginRight:'104px'}} src={`${__webpack_public_path__}static/${bodyImg}`}/>
                    <div style={{display:'flex', flexDirection:'column'}}>
                    <div style={{width: "460px", height: "184px"}}>
                        <Typography component="h2" fontSize="38px" fontWeight={600}>{bodyTitle}</Typography>
                    </div>
                    <div style={{width: "460px", height: "192px"}}>
                    <Typography component="p" fontSize="16px">{secondaryInfo}</Typography>
                    </div>
                    </div>
                </Secondary>
            </Wrapper>

            <div className="gallarySlider">
            <GallarySlider array={imgArr}></GallarySlider>
            </div>

            <Wrapper>
            <SecondaryContent>
            <div style={{marginRight:'104px'}}><Typography component="p" fontSize="16px">{secondaryInfo}</Typography></div>
            <div>
                <SimilarSections />
            </div>
            </SecondaryContent>
            </Wrapper>

            <SocialShareWrapper data-cy="social-link">
                <SocialShare
                    title="Мы в социальных сетях"
                    alt="Мы в соцсетях"
                    vk_link={links?.vk}
                    instagram_link={links?.instagram}
                    facebook_link={links?.facebook}
                    ok_link={links?.ok}
                    twitter_link={links?.twitter}
                />
            </SocialShareWrapper>

            <ReachGoalsWrapper>
                <ReachGoals
                    title="Достигайте своих целей"
                    buttonText="Записаться"
                    onClick={toggleModal}
                />
                <ModalProvider>
                    <ModalInfo 
                        showModal={isOpen} 
                        toggleModal={toggleModal} 
                        setPopup={popupToShow}
                        
                    />

                <Popup 
                    appear={ popup === 'success' }
                    image={'ico-success.svg'}
                    title={'Здорово!'}
                    body={'Наш менеджер свяжется с вами по указанному телефону'} 
                    setPopup={popupToShow} />

                <Popup 
                    appear={ popup === 'fail' }
                    image={'ico-unsuccess.svg'}
                    title={'Ой!'}
                    body={`Что-то пошло не так. Повторите попытку позже.`} 
                    setPopup={popupToShow} />
            </ModalProvider>
            </ReachGoalsWrapper>
            <Footer />
        </>
    )
}

const mapStateToProps = (state) => createStructuredSelector({
    title: getSectionTitle,
    bodyContent: getBodyContent, 
    image: getImg, 
    subTitle: getSectionSubtitle,
    secondaryInfo: getSecondaryInfo,
    imgArr: getImgArr, 
    links: getSectionLinks,
    bodyImg: getBodyImg, 
    bodyTitle: getBodyTitle, 
    isLoading: getSectionDataLoading,
    isError: getError,
})

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    fetchSectionData
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(SectionPage)