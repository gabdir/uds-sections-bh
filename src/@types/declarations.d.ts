declare module '*.png';

declare module '*.jpg';

declare module '*.jpeg';

declare module '*.svg';

declare module '*.ico';

declare module "*.module.css";

declare module "*.css";

declare module '*.scss';

declare var __webpack_public_path__: string;